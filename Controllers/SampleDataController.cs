﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace Vue2Spa.Controllers
{
    [Route("api/[controller]")]
    public class SampleDataController : Controller
    {
        private Product[] products = {
            new Product
            {
                ID =1,Name="Tomato Soup",Category="Groceries", Price=1
            },
            new Product
            {
                ID =2,Name="Yo-yo" ,Category="Toys", Price=3.75M
            },
            new Product
            {
                ID =3, Name="Hammer",Category="Hardware",Price=16.99M
            }
        };

        [HttpGet("[action]")]
        public IEnumerable<Product> GetAllProducts()
        {
            return products;
        }

        [HttpGet("[action]={product}")]
        public IEnumerable<Product> Search(string product)
        {
            List<Product> list = new List<Product>();
            foreach (var item in products)
            {
                if (item.Name.ToLower().Contains(product.Trim().ToLower()))
                {
                    list.Add(item);
                }
            }
            return list.ToList();
        }
    }

    public class Product
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public decimal Price { get; set; }
    }
}