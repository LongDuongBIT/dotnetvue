# DotNetVue
    This repo contains an aspnetcore + Vue.js 2 starter template (VS2017).
    The template is based on the original starter templates (Angular, Knockout, React, Aurelia), which can be found at "https://github.com/aspnet/JavaScriptServices/tree/dev/templates/VueSpa"

# Features
    - ASP.Net Core
        . Web API
    - VueJS 2
        . Vuex (State Store)
    - Webpack 2
        . HMR (Hot Module Replacement/Reloading)
    - Bootstrap
    - ... and more

# Prerequisites
    - NodeJS > 6
    - VS2017
    - Dotnet Core

# Installation
    - "git clone https://github.com/LongDuongBIT/DotNetVue.git
    - "cd DotNetVue"
    - "dotnet restore" to restore dotnet dependencies
    - "yarn install" (recommended) or "npm install" to restore Node dependencies
    - "npm run dev" to run the app or double click on VueSPA.sln to open app in VS2017 and hit F5
    - Browse to http://localhost:5000 and enjoy!
